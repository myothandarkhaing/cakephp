<?php
class Event extends AppModel {
    var $name = 'Event';
    var $hasMany = array(
        'Comment' => array(
            'className' => 'Comment',
            'foreignKey' => 'user_id',
            'conditions' => array('Comment.comment'=>'Event'),
        ),
    );
}