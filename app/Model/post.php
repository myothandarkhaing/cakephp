<?php

class Post extends AppModel {

	public $validate = array(
    	'title' => array(
        'rule' => 'notBlank'
        ),
        'body' => array(
            'rule' => 'notBlank'
        ),
        'comment' => array(
            'rule' => 'notBlank'

            )
    );

    var $name = 'Post';
    var $hasMany = array(
        'Comment' => array(
            'className' => 'Comment',
            'foreignKey' => 'user_id',
              ),
    );
        public function isOwnedBy($post, $user) {
        return $this->field('id', array('id' => $post, 'user_id' => $user)) !== false;
    }
}