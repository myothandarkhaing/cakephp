<?php

class PostsController extends AppController {

    public $helpers = array('Html', 'Form', 'Flash','Paginator');
    public $components = array('Flash','Paginator');
    var $name = 'Posts';

    public function index() {

        $this->Paginator->settings = array(
                 'Post' => array(
            'paramType' => 'querystring',
                'limit' => 5,
                'order' => array(
              'Post.id' => 'asc'
                )
            )
        );
        $this->set('posts', $this->Paginator->paginate());
        $id= $this->Auth->user('id');
        $this->set('userid',$id);
    }

    public function view($id=null) {

        if (!$id) {
            throw new NotFoundException(__('Invalid post'));
        }
        $post = $this->Post->findById($id);

        if (!$post) {
            throw new NotFoundException(__('Invalid post'));
        }
        $this->set('post', $post);
    
        if (!empty($this->request->data['Comment'])) {
            $this->request->data['Comment']['user_id'] = $id; 
            $this->Post->Comment->create(); 
            $this->request->data['Comment']['commentposter'] = $this->Auth->user('username');
            if ($this->Post->Comment->save($this->data)) {
                $this->Flash->success(__('The Comment could be saved.', true),'success');
                $this->redirect(array('action'=>'view',$id));
            }
            $this->Flash->success(__('The Comment could not be saved. Please, try again.', true),'warning');
        }
      // set the view variables
        $post = $this->Post->read(null, $id); // contains $post['Comments']
        $this->set(compact('post'));
    }

    public function isAuthorized($user) {

        if ($this->action === 'add') {
            return true;
        }    
        if (in_array($this->action, array('edit', 'delete'))) {

            $postId = (int) $this->request->params['pass'][0];
            if ($this->Post->isOwnedBy($postId, $user['id'])) {
                return true;
            } else {
                  $this->Flash->error(__('Unable to delete and edit your post.')); 
                  return $this->redirect($this->Auth->logout());
            }
        }
        return parent::isAuthorized($user);
    }

    public function add() {

        if(!empty($this->request->data)) {

            if ($this->request->is('post')) {

                $this->Post->create();
                $this->request->data['Post']['user_id'] = $this->Auth->user('id');
                $posterid=$this->Auth->user('id');

            if(!empty($this->request->data['Post']['Image']['name'])) {

                $file = $this->request->data['Post']['Image']; 
                $ext = substr(strtolower(strrchr($file['name'], '.')), 1); 
                $arr_ext = array('jpg', 'jpeg', 'gif','png'); 
                
                if(in_array($ext, $arr_ext)) {                                
                    move_uploaded_file($file['tmp_name'], WWW_ROOT . '/img/' . $file['name']);
                    $this->request->data['Post']['image_url'] = '/img/'.$file['name'];
                }
            }   
            if ($this->Post->save($this->request->data)) {
                $this->Flash->success(__('Your post has been saved.'));

                return $this->redirect(array('action' => 'index'));     

            } else {
            $this->Flash->error(__('Unable to add your post.'));
            }
          }
        }
    }

    public function edit($id = null) {

        if (!$id) {
            throw new NotFoundException(__('Invalid post'));
        }
        $post = $this->Post->findById($id);

        if (!$post) {
            throw new NotFoundException(__('Invalid post'));
        }

        if ($this->request->is(array('post', 'put'))) {
            $this->Post->id = $id;
            if(!empty($this->request->data['Post']['Image']['name'])) {
                $file = $this->request->data['Post']['Image']; //put the data into a var for easy use
                $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                $arr_ext = array('jpg', 'jpeg', 'gif','png'); //set allowed extensions

                if(in_array($ext, $arr_ext)) { 

                    move_uploaded_file($file['tmp_name'], WWW_ROOT . '/img/' . $file['name']);
                    $this->request->data['Post']['image_url'] = '/img/'.$file['name'];
                }
            }   
            if ($this->Post->save($this->request->data)) {
                $this->Flash->success(__('Your post has been saved.'));
                return $this->redirect(array('action' => 'index'));     
            } 
            else {
                 $this->Flash->error(__('Unable to add your post.'));
            }
        }  
        if (!$this->request->data) {
            $this->request->data = $post;
        }
    }

    public function delete($id) {

        if ($this->request->is('get')) {
            throw new MethodNotAllowedException();
        }

        if ($this->Post->delete($id)) {
            $this->Flash->success(
                 __('The post with id: %s has been deleted.', h($id))
            );
        } 
        else {
            $this->Flash->error(
            __('The post with id: %s could not be deleted.', h($id))
          );
        }

         return $this->redirect(array('controller' => 'posts','action' => 'index'));
    }

    public function search() {
        $id=$this->request->data['Post']['Search by Id'];
        $title=$this->request->data['Post']['Search by Title'];
        $body=$this->request->data['Post']['Search by Body'];
        if($id == "" && $title == "" && $body == ""){
            $this->Flash->error(__('Please fill you want to search'));
            return $this->redirect(array('action' => 'index'));
        }
        else if($id !="" && $title == "" && $body == ""){
            $results = $this->Post->find('all', array(
          'conditions' => array('Post.id LIKE' => "%". $id ."%")));
        }
        else if($id == "" && $title != "" && $body == ""){
            $results = $this->Post->find('all', array(
          'conditions' => array('Post.title LIKE' => "%". $title ."%")));       
        }
        else if($id == "" && $title == "" && $body != ""){
           $results = $this->Post->find('all', array(
          'conditions' => array('Post.body LIKE' => "%". $id ."%")));
        }
        $this->set('results',$results);
    }
}