<?php
    echo $this->Html->link(
    'Logout', array('controller'=>'users','action' => 'logout')
    );
?>

<table>

<tr>
    <td> <?php echo $this->Form->create('Post',array('controller'=>'posts','url'=>'Search')); ?> </td>
    <td> <?php echo $this->Form->input('Search by Id',array('type'=>'text' ,'rows' => '1')); ?> </td>
    <td><?php echo $this->Form->input('Search by Title',array('type'=>'text','rows' => '1')); ?> </td>
    <td><?php echo $this->Form->input('Search by Body',array('type'=>'text','rows' => '1')); ?> </td>
</tr>

<tr>
    <td><?php echo $this->Form->end('Search'); ?> </td>
</tr>

</table>

<p><?php echo $this->Html->link('Add New Topic', array('action' => 'add')); ?></p>
<table>
    <tr>
        <th>Id</th>
        <th>Topic</th>
        <th>Actions</th>
        <th>Created</th>
        <th>Modified</th>

    </tr>

<!-- Here's where we loop through our $posts array, printing out post info -->

<?php foreach ($posts as $post): ?>
    <tr>
        <td><?php echo $post['Post']['id']; ?></td>
        <td>
            <?php
                echo $this->Html->link(
                    $post['Post']['title'],
                    array('action' => 'view', $post['Post']['id'])
                );
            ?>
        </td>
        <td>
            <?php
                
                echo $this->Form->postLink(
                    'Delete',
                    array('action' => 'delete', $post['Post']['id']),
                    array('confirm' => 'Are you sure you want to delete?')
                );
            ?>
            <?php
                echo $this->Html->link(
                    'Edit', array('action' => 'edit', $post['Post']['id'])
                );
            ?>
        </td>
        <td>
            <?php echo $post['Post']['created']; ?>
        </td>
        <td>
            <?php echo $post['Post']['modified']; ?>
        </td>
    </tr>
<?php endforeach; ?>
</table>
    
<div>
    <?php echo $this->Paginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}'))); ?>
</div>

<div class="paging">
    <?php
        echo $this->Paginator->prev('< ' . __('Previous Page'), array(), null, array('class' => 'prev disabled'));
        echo $this->Paginator->next(__('Next Page') . ' >', array(), null, array('class' => 'next disabled'));
    ?>
</div>

